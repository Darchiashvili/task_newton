# Server - Workers demo project

Requirements:
 - RabbitMQ server
 - PostgreSQL server

Before run:
 - Run `pip install -r requirements.txt`

Instructions to run:
 - Run django rest api in server directory using `python manage.py runserver`
 - Run nameko event dispatcher in client directory using `nameko run dispatcher`
 - Run nameko event listeners (Clients) in client directory using `python run_clients.py [num]`

How it works:
 1. Django allows you to add tasks trough the RestAPI
 2. After receiving new task django then sends request to the dispatcher to queue.
 3. Clients launched from the `run_clients.py` executes given tasks.

Technologies used:
 - Django
 - DjangoRestFramework
 - Nameko + RabbitMQ

