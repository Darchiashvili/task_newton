from nameko.events import EventDispatcher
from nameko.rpc import rpc


class ServiceDispatcher:
    """ Event dispatching service. """
    name = "dispatcher"

    dispatch = EventDispatcher()

    @rpc
    def dispatching_method(self, payload):
        if 'operation' in payload:
            operation = payload['operation']
            if operation == 'execute_task':
                task_id = payload['id']
                print(f'Dispatching event to execute task with id: {task_id}')
                self.dispatch(operation, task_id)
