import math
from nameko.events import event_handler, SINGLETON
from django_integration.extension import DjangoORM


class ServiceClient:
    """ Event listening service. """
    name = "client"

    models = DjangoORM()

    @event_handler("dispatcher", "execute_task", handler_type=SINGLETON, reliable_delivery=True)
    def handle_event(self, task_id):
        print("client received task id: ", task_id)
        try:
            task = self.models.Task.objects.get(pk=task_id)
            res = None
            error_message = None
            if len(task.arguments):
                try:
                    if task.function == 'sum':
                        res = math.fsum(task.arguments)
                    elif task.function == 'subtract':
                        res = task.arguments[0]
                        for el in task.arguments[1:]:
                            res -= el
                    elif task.function == 'multiply':
                        res = task.arguments[0]
                        for el in task.arguments[1:]:
                            res *= el
                    elif task.function == 'divide':
                        res = task.arguments[0]
                        for el in task.arguments[1:]:
                            res /= el
                except Exception as e:
                    error_message = str(e)
            else:
                error_message = 'No arguments specified'
            task_result = self.models.TaskResult(task=task)
            if error_message is None:
                task_result.result_value = res
            else:
                task_result.error_message = error_message
            task_result.save()
            task.done = True
            task.save()
        except self.models.Task.DoesNotExist:
            pass
