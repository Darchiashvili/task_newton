import os
import sys
import django

from nameko.extensions import DependencyProvider
from django_integration.settings import DJANGO_PATH, DJANGO_SETTINGS_MODULE


class DjangoORM(DependencyProvider):

    def setup(self):
        sys.path.append(os.path.abspath(DJANGO_PATH))
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', DJANGO_SETTINGS_MODULE)
        django.setup()

    def get_dependency(self, worker_ctx):
        from django.apps import apps
        from django.conf import settings

        apps_config = map(apps.get_app_config, settings.NAMEKO_APPS)
        models = type('NonExistingClass_', (), {})

        for config in apps_config:
            for model in config.get_models():
                setattr(models, model.__name__, model)
        return models

    def worker_teardown(self, worker_ctx):
        from django.db import connections
        connections.close_all()
