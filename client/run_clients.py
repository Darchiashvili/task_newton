import subprocess
import sys
import signal


CLIENT_PROCESSES = list()


def singlal_handler(sig, frame):
    print('Gracefully killing clients...')
    for proc in CLIENT_PROCESSES:
        proc.send_signal(signal.SIGTERM)
    sys.exit(0)


if __name__ == '__main__':
    signal.signal(signal.SIGINT, singlal_handler)
    if len(sys.argv) != 2 or not sys.argv[1].isdigit():
        print("Invalid or no arguments.")
        sys.exit(1)
    client_qty = int(sys.argv[1])
    for num in range(client_qty):
        CLIENT_PROCESSES.append(subprocess.Popen(["nameko", "run", "client"]))
    while True:
        for proc in CLIENT_PROCESSES:
            proc.communicate()
