from django.db import models
from django.contrib.postgres import fields


ALLOWED_FUNCTIONS = [
    ('sum', 'Sum'),
    ('subtract', 'Subtract'),
    ('divide', 'Divide'),
    ('multiply', 'Multiply'),
]


class Task(models.Model):
    create_date = models.DateTimeField(verbose_name='Create Date', auto_now_add=True)
    function = models.CharField(
        max_length=8,
        verbose_name='Function',
        choices=ALLOWED_FUNCTIONS,
        default='sum',
    )
    arguments = fields.ArrayField(
        models.IntegerField(),
        verbose_name='Arguments',
    )
    done = models.BooleanField(verbose_name='Done', default=False, blank=True)

    def __repr__(self):
        return f'Task - operation [{self.function}]'


class TaskResult(models.Model):
    task = models.ForeignKey(
        Task,
        on_delete=models.CASCADE,
        verbose_name='Task',
        related_name='Results'
    )
    result_value = models.IntegerField(verbose_name='Result', null=True, blank=True)
    error_message = models.TextField(blank=True, null=True)

    def __repr__(self):
        return f'Result for task - {repr(self.task)}'
