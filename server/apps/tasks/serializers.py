from django_nameko import get_pool
from rest_framework import serializers

from apps.tasks.models import Task, TaskResult


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('id', 'create_date', 'function', 'done')


class TaskCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('function', 'arguments')

    def create(self, validated_data):
        obj = super().create(validated_data)
        with get_pool().next() as ec:
            ec.dispatcher.dispatching_method({
                'operation': 'execute_task',
                'id': obj.id
            })
        return obj


class TaskResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskResult
        fields = '__all__'
