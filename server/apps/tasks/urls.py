from django.urls import path, include
from rest_framework import routers
from apps.tasks.views import TaskViewSet

router = routers.DefaultRouter()
router.register(r'tasks', TaskViewSet)

app_name = 'apps.tasks'
urlpatterns = [
    path('', include(router.urls)),
]
