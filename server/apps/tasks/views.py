from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin
from rest_framework.response import Response

from apps.tasks.models import Task, TaskResult
from apps.tasks.serializers import TaskSerializer, TaskResultSerializer, TaskCreateSerializer


class TaskViewSet(CreateModelMixin, ListModelMixin, RetrieveModelMixin, viewsets.GenericViewSet):
    """
    API endpoint that allows text to be viewed.
    """
    queryset = Task.objects.order_by('done', '-create_date')
    serializer_class = TaskSerializer
    create_serializer_class = TaskCreateSerializer
    permission_classes = []

    def get_serializer_class(self):
        if self.action == 'create':
            return self.create_serializer_class
        return super().get_serializer_class()

    @action(detail=True, methods=['get'])
    def result(self, request, pk, *args, **kwargs):
        try:
            task = Task.objects.get(pk=pk)
        except task.DoesNotExist:
            return Response({'message': 'The sentence does not exists.'},
                            status=status.HTTP_404_NOT_FOUND)
        other_sentences = TaskResult.objects.filter(task__in=[task])
        serializer = TaskResultSerializer(other_sentences, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
